
var initialLocation;
var portland = new google.maps.LatLng(45.498807, -122.674406);
var browserSupportFlag =  new Boolean();
var geocoder;
var map; 
var boxpolys;
var PositionMarker ;
var infowindow = new google.maps.InfoWindow();
var marker;
var follow;
var boxes;
var alert_test;
var Location;
var infoBubble = new InfoBubble({maxWidth: 300});

/*
function locFn(pos) {
      Location = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(Location);
      var blueIcon = new GIcon(G_DEFAULT_ICON);
      blueIcon.image = "http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
      markerOptions = { icon:blueIcon };
      map.addOverlay(new GMarker(Location, markerOptions));
}
*/
  var myOptions = {
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
 var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
function initialize() {

    geocoder = new google.maps.Geocoder();
      routeBoxer = new RouteBoxer();
      
      directionService = new google.maps.DirectionsService();
      directionsRenderer = new google.maps.DirectionsRenderer({ map: map });      
  // Try W3C Geolocation (Preferred)
  if(navigator.geolocation) {
    browserSupportFlag = true;
    navigator.geolocation.getCurrentPosition(function(position) {
      initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(initialLocation);
      PositionMarker = new google.maps.Marker({
	//    position: initialLocation,
	    map: map,
	    icon:  "http://oa-samples.googlecode.com/svn-history/r73/trunk/presentations/gdd-2010/saopaulo/talks/maps/my-location.png"
	});
    }, function() {
      handleNoGeolocation(browserSupportFlag);
    });
    watchID = navigator.geolocation.watchPosition(
    function(position){
      Location = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
//      alert(Location);
//      alert(Location.lat());
//      alert(Location.lng());
	  if(!isEmpty(boxes)){
	    if(insideboxes(boxes, Location) == false){
	      route()
	    }
	  }
  if(follow == true){    map.setCenter(Location); }
     // var blueIcon = new GIcon(G_DEFAULT_ICON);
    //  blueIcon.image = "http://www.google.com/mobile/images/gmm/blue_dot.jpg";
    //  markerOptions = { icon:blueIcon };
	PositionMarker.setPosition(Location);

   // alert(this_box);
  //	alert(boxes);
 //     map.addOverlay(new GMarker(Location, markerOptions));
  //alert('test');

    }, function() {
      handleNoGeolocation(browserSupportFlag);
    },
          { enableHighAccuracy: true });

  // Try Google Gears Geolocation
  } else if (google.gears) {
    browserSupportFlag = true;
    var geo = google.gears.factory.create('beta.geolocation');
    geo.getCurrentPosition(function(position) {
      initialLocation = google.maps.LatLng(position.latitude,position.longitude);
      map.setCenter(initialLocation);
    }, function() {
      handleNoGeoLocation(browserSupportFlag);
    });
  // Browser doesn't support Geolocation
  } else {
    browserSupportFlag = false;
    handleNoGeolocation(browserSupportFlag);
  }
  
  function handleNoGeolocation(errorFlag) {
    if (errorFlag == true) {
      alert("Geolocation service failed.");
      initialLocation = portland;
    } else {
      alert("Your browser doesn't support geolocation. We've placed you in portland.");
      initialLocation = portland;
    }
    map.setCenter(initialLocation);
  }
}

  function codeAddress(address, title, phone) {
    
   // alert(address);
  //  var geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': address }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
    //    map.setCenter(results[0].geometry.location);
   // alert(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
	    html: title + "<br>" + address + "<br>" + '<a href="wtai://wp/mc;'+ phone + '">' + phone +  '</a><div name="'+ results[0].geometry.location +'" id="directions">',
	    title: title
        });

	google.maps.event.addListener(marker, 'click', function () {
	// where I have added .html to the marker object.
	infowindow.setContent(this.html);
	infowindow.open(map, this);
	});

      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
  
  function generate_marker_html(title, phone, address){
    var html = new Object();
    	html.info =  '<div name="'+'" id="directions" class="directions" style=""><div id="addresss">'+ title + '<br>' + address + ' ' + '<br/>'+ '<a href="wtai://wp/mc;'+ phone + '">' + phone +  '</a>' + '</div></div>';
	html.directions =  '<div name="'+'" id="directions" class="directions" style=""><button class="direct" id="drive" onclick="mzg(\'1\');"/><button class="direct" id="bike" onclick="mzg(\'2\');" /><button class="direct" id="walk"  onclick="mzg(\'3\');" /></div>';
	html.deals =  '<div name="'+'" id="directions" class="directions" style=""><div id="addresss">'+ title + '<br>' + address + ' ' + '<br/>'+ '<a href="wtai://wp/mc;'+ phone + '">' + phone +  '</a>' + '</div><button class="direct" id="drive" onclick="mzg(\'1\');"/><button class="direct" id="bike" onclick="mzg(\'2\');" /><button class="direct" id="walk"  onclick="mzg(\'3\');" /></div>';
		
	$(".directions").css('font-size', Math.round(($(window).width() / 50)) + 'px');
// 	alert(Math.round(($(window).width() / 50)) + 'px');
	return html;
  }
  
  function place_marker_html(lat, lng, html){
	var marker_location = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
            map: map,
            position: marker_location,
	    html: html.info,
// 	    title: title,
// 	    icon:  "http://oa-samples.googlecode.com/svn-history/r73/trunk/presentations/gdd-2010/saopaulo/talks/maps/my-location.png"
        });
//	alert(dump(marker));	
	place_infowindow_marker(marker, html);
  }
    
    
  function place_infowindow_marker(marker, data) {
    var width = $(window).width();
    var height = $(window).height(); 
    var max_width;
    if(width > height){
     max_width = ((width * 0.9) / 2);
    }
    else{
     max_width = ((width * 0.8)); 
    }
	
	infoBubble = null;
	var infoBubble = new InfoBubble({maxWidth: 300});


//         google.maps.event.addDomListener(document.getElementById('update'),
//             'click', updateStyles);
//         google.maps.event.addDomListener(document.getElementById('add'),
//             'click', addTab);
//         google.maps.event.addDomListener(document.getElementById('update-tab'),
//             'click', updateTab);
//         google.maps.event.addDomListener(document.getElementById('remove'),
//             'click', removeTab);
//         google.maps.event.addDomListener(document.getElementById('open'),
//             'click', function() {
//             infoBubble2.open();
//         });
//         google.maps.event.addDomListener(document.getElementById('close'),
//             'click', function() {
//           infoBubble2.close();
//         });

	
    	google.maps.event.addListener(marker, 'click', function () {
// 	infowindow.setOptions({maxWidth: max_width});
// 	infowindow.setContent(this.html);

// 	infoBubble.addTab('Directions', this.html);
// 	infoBubble.addTab('Deals', this.html);
// 	infowindow.open(map, this);
	if (!infoBubble.isOpen()) {
	  infoBubble.removeTab(0);
	  infoBubble.removeTab(0);
	    infoBubble.addTab('Info', data.info);
	    infoBubble.addTab('Directions', data.directions);
            infoBubble.open(map, this);
          }
// 	infoBubble.open(map, this);
	$('#map_canvas').resize(function() {
// 	infowindow.setOptions({maxWidth: max_width});
// 	infowindow.setContent(this.html);
// 	infowindow.open(map, this);
	   
	});
// 	alert(max_width);
	});
  }
  
      function addTab() {
        var title = document.getElementById('tab-title').value;
        var content = document.getElementById('tab-content').value;

        if (title != '' && content != '') {
          infoBubble.addTab(title, content);
        }
      }

      function updateTab() {
        var index = document.getElementById('tab-index-update').value;
        var title = document.getElementById('tab-title-update').value;
        var content = document.getElementById('tab-content-update').value;

        if (index) {
          infoBubble.updateTab(index, title, content);
        }
      }

      function removeTab() {
        var index = document.getElementById('tab-index').value;
        infoBubble.removeTab(index);
      }
      
      function updateStyles() {
        var shadowStyle = document.getElementById('shadowstyle').value;
        infoBubble.setShadowStyle(shadowStyle);

        var padding = document.getElementById('padding').value;
        infoBubble.setPadding(padding);

        var borderRadius = document.getElementById('borderRadius').value;
        infoBubble.setBorderRadius(borderRadius);

        var borderWidth = document.getElementById('borderWidth').value;
        infoBubble.setBorderWidth(borderWidth);

        var borderColor = document.getElementById('borderColor').value;
        infoBubble.setBorderColor(borderColor);

        var backgroundColor = document.getElementById('backgroundColor').value;
        infoBubble.setBackgroundColor(backgroundColor);

        var maxWidth = document.getElementById('maxWidth').value;
        infoBubble.setMaxWidth(maxWidth);

        var maxHeight = document.getElementById('maxHeight').value;
        infoBubble.setMaxHeight(maxHeight);

        var minWidth = document.getElementById('minWidth').value;
        infoBubble.setMinWidth(minWidth);

        var minHeight = document.getElementById('minHeight').value;
        infoBubble.setMinHeight(minHeight);

        var arrowSize = document.getElementById('arrowSize').value;
        infoBubble.setArrowSize(arrowSize);

        var arrowPosition = document.getElementById('arrowPosition').value;
        infoBubble.setArrowPosition(arrowPosition);

        var arrowStyle = document.getElementById('arrowStyle').value;
        infoBubble.setArrowStyle(arrowStyle);
      }

  
  function codeCoordinate(lat, lng, title, phone, address) {
	//    	map.setCenter(results[0].geometry.location);
	//	alert(results[0].geometry.location);
	var marker_location = new google.maps.LatLng(lat, lng);
	
	var html = '';
	html =  '<div name="'+'" id="directions" style=" font-size: 1.4em;"><div id="addresss">'+ title + '<br>' + address + ' ' + '<br/>'+ '<a href="wtai://wp/mc;'+ phone + '">' + phone +  '</a>' + '</div><button class="direct" id="drive" onclick="mzg(\'1\');"/><button class="direct" id="bike" onclick="mzg(\'2\');" /><button class="direct" id="walk"  onclick="mzg(\'3\');" /></div>';
        marker = new google.maps.Marker({
            map: map,
            position: marker_location,
	    html: html,
	    title: title,
// 	    icon:  "http://oa-samples.googlecode.com/svn-history/r73/trunk/presentations/gdd-2010/saopaulo/talks/maps/my-location.png"

        });
	//marker.setPosition(marker_location);
//	alert(dump(marker));
	
	google.maps.event.addListener(marker, 'click', function () {
	// where I have added .html to the marker object.
	infowindow.setContent(this.html);
	infowindow.open(map, this);
	});

      
    
  }

    function route(to, from, method) {
      // Clear any previous route boxes from the map
 //     clearBoxes();
      //      alert(Location);

      // Convert the distance to box around the route from miles to km
      distance = 0.02;
      if(method = 1){var mode = google.maps.DirectionsTravelMode.DRIVING}
      if(method = 2){var mode = google.maps.DirectionsTravelMode.BICYCLING}
      if(method = 3){var mode = google.maps.DirectionsTravelMode.WALKING}


      var request = {
        origin: from,
        destination: to,
        travelMode: mode
      }
      
      // Make the directions request
      directionService.route(request, function(result, status) {
// 	alert(dump(result));
        if (status == google.maps.DirectionsStatus.OK) {
          directionsRenderer.setDirections(result);
          
          // Box around the overview path of the first route
          var path = result.routes[0].overview_path;
          var boxes = routeBoxer.box(path, distance);
	  //alert(boxes);
	  window.boxes = boxes; 
  //        drawBoxes(boxes);
        } else {
     //     alert("Directions query failed: " + status);
        }
      });
    }
    
    // Draw the array of boxes as polylines on the map
    function drawBoxes(boxes) {
      boxpolys = new Array(boxes.length);
   //   insidebox(boxes);
      var color = "#000000";
      for (var i = 0; i < boxes.length; i++) {
 //     if(insidebox(boxes[i], Location)){ color = "#00ff00";} else { color = "#ff0000";}
        boxpolys[i] = new google.maps.Rectangle({
          bounds: boxes[i],
          fillOpacity: 0,
          strokeOpacity: 1.0,
          strokeColor: color,
          strokeWeight: 1,
          map: map
        });

	google.maps.event.addListener(boxpolys[i], 'click', function () {
	// where I have added .html to the marker object.
	});
      }
    }
    
    // Clear boxes currently on the map
    function clearBoxes() {
      if (boxpolys != null) {
        for (var i = 0; i < boxpolys.length; i++) {
          boxpolys[i].setMap(null);
        }
      }
      boxpolys = null;
    }

function insideboxes(box, _position){
      var _box;
      var a = 0;
      var b = 0;
      var c = 0;
      var x;
      var y;
      var z;
      var temp = new Array();
      var temp2 = false;
       for(x in box){
	 temp[0] = new Array();
	 temp[1] = new Array();
	temp[0][0]  = box[x].Z.b;
	temp[1][0]  = box[x].aa.b;
	temp[0][1]  = box[x].Z.d;
	temp[1][1]  = box[x].aa.d;


      if((_position.lat() > temp[0][0] && _position.lat() < temp[0][1]) && (_position.lng() > temp[1][0] && _position.lng() < temp[1][1])){
      temp2 = true;
      }
	
        }
return temp2;
}

function insidebox(box, _position){
      var _box;
      var a = 0;
      var b = 0;
      var c = 0;
      var x;
      var y;
      var z;
      var temp = new Array();
      var temp2;

//	alert(dump(box));
	 temp[0] = new Array();
	 temp[1] = new Array();
	temp[0][0]  = box.Z.b;
	temp[1][0]  = box.aa.b;
	temp[0][1]  = box.Z.d;
	temp[1][1]  = box.aa.d;
//alert(temp[1][1]);
//      alert(_position.lat());
//      alert(Location.latitude());
//      alert(Location.longitude());
//alert("minlat:"+temp[0][0]+"maxlat:"+temp[0][1]+"minlng:"+temp[1][0]+"maxlng:"+temp[1][1]+"Location:"+_position);


      if((_position.lat() > temp[0][0] && _position.lat() < temp[0][1]) && (_position.lng() > temp[1][0] && _position.lng() < temp[1][1])){
      temp2 = true;
      }
//alert(dump(box));
return temp2;
}


function dump(arr,level) {
var dumped_text = "";
if(!level) level = 0;

//The padding given at the beginning of the line.
var level_padding = "";
for(var j=0;j<level+1;j++) level_padding += "    ";

if(typeof(arr) == 'object') { //Array/Hashes/Objects
 for(var item in arr) {
  var value = arr[item];
 
  if(typeof(value) == 'object') { //If it is an array,
   dumped_text += level_padding + "'" + item + "' ...\n";
   dumped_text += dump(value,level+1);
  } else {
   dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
  }
 }
} else { //Stings/Chars/Numbers etc.
 dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
}
return dumped_text;
} 


function mzg(info) {
//alert(Location);
// route($('#directions').attr('name'), Location, info)
}


function isEmpty(obj) {
if (typeof obj == 'undefined' || obj === null || obj === '') return true;
if (typeof obj == 'number' && isNaN(obj)) return true;
if (obj instanceof Date && isNaN(Number(obj))) return true;
return false;
}



