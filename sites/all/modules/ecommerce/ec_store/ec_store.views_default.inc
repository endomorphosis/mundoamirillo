<?php

/**
 * @file
 * Default views for ec_store module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function ec_store_views_default_views() {
  /*
   * View 'customer_transaction_list'
   */
  $view = new view;
  $view->name = 'customer_transaction_list';
  $view->description = 'List of transactions by customer';
  $view->tag = 'ec_store';
  $view->base_table = 'ec_transaction';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'txnid' => array(
      'label' => 'Id',
      'exclude' => 0,
      'id' => 'txnid',
      'table' => 'ec_transaction',
      'field' => 'txnid',
      'relationship' => 'none',
    ),
    'gross' => array(
      'label' => 'Total',
      'exclude' => 0,
      'id' => 'gross',
      'table' => 'ec_transaction',
      'field' => 'gross',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_transaction',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'changed' => array(
      'label' => 'Changed',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'changed',
      'table' => 'ec_transaction',
      'field' => 'changed',
      'relationship' => 'none',
    ),
    'workflow' => array(
      'label' => 'Order Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'workflow',
      'table' => 'ec_workflow_statuses',
      'field' => 'description',
      'relationship' => 'none',
    ),
    'allocation' => array(
      'label' => 'Payment Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'allocation',
      'table' => 'ec_transaction',
      'field' => 'allocation',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_transaction',
      'field' => 'operations',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'ecid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '%1',
      'default_argument_type' => 'user',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'empty',
      'break_phrase' => 1,
      'not' => 0,
      'id' => 'ecid',
      'table' => 'ec_customer',
      'field' => 'ecid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'deadwood_category' => 0,
        'deadwood_item' => 0,
        'amazon_book' => 0,
        'file' => 0,
        'page' => 0,
        'product' => 0,
        'product_some' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'ec_customer',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'txnid' => 'txnid',
      'gross' => 'gross',
      'created' => 'created',
      'changed' => 'changed',
      'allocation' => 'allocation',
      'workflow' => 'workflow',
      'operations' => 'operations',
    ),
    'info' => array(
      'txnid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'gross' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'allocation' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'workflow' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'operations' => array(
        'separator' => '',
      ),
    ),
    'default' => 'txnid',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'user/%/store/transactions');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'Transactions',
    'description' => '',
    'weight' => '-10',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'tab',
    'title' => 'Store',
    'description' => '',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $views[$view->name] = $view;

  /*
   * View 'ec_transaction_in_picking'
   */
  $view = new view;
  $view->name = 'ec_transaction_in_picking';
  $view->description = 'List of transactions ready to be shipped.';
  $view->tag = 'ec_store';
  $view->base_table = 'ec_transaction';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'txnid' => array(
      'label' => 'Transaction id',
      'exclude' => 0,
      'id' => 'txnid',
      'table' => 'ec_transaction',
      'field' => 'txnid',
      'relationship' => 'none',
    ),
    'fullname' => array(
      'label' => 'Name',
      'include_multiline_address' => 0,
      'exclude' => 0,
      'id' => 'fullname',
      'table' => 'ec_transaction_address',
      'field' => 'fullname',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '[title]',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'title',
      'table' => 'ec_transaction_product',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'qty' => array(
      'label' => 'Quantity',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'qty',
      'table' => 'ec_transaction_product',
      'field' => 'qty',
      'relationship' => 'none',
    ),
    'price' => array(
      'label' => 'Price',
      'exclude' => 0,
      'id' => 'price',
      'table' => 'ec_transaction_product',
      'field' => 'price',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'or',
      'value' => array(
        'billing' => 'billing',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'ec_transaction_address',
      'field' => 'type',
      'add_table' => 'ec_transaction_addrress',
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'workflow' => array(
      'operator' => 'in',
      'value' => array(
        '10' => '10',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'workflow',
      'table' => 'ec_workflow_statuses',
      'field' => 'workflow',
      'relationship' => 'none',
    ),
    'shippable_1' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'shippable_1',
      'table' => 'ec_transaction_product',
      'field' => 'shippable',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'In picking');
  $handler->override_option('style_plugin', 'bulk_multi');
  $handler->override_option('style_options', array(
    'grouping' => 'txnid',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'summary' => '',
    'columns' => array(
      'txnid' => 'txnid',
      'fullname' => 'fullname',
      'title' => 'title',
      'qty' => 'qty',
      'price' => 'price',
    ),
    'info' => array(
      'txnid' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'fullname' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'title' => array(
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'qty' => array(
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'price' => array(
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
    ),
    'default' => '-1',
    'execution_type' => '1',
    'max_performance' => 0,
    'display_type' => '1',
    'hide_selector' => 0,
    'preserve_selection' => 1,
    'display_result' => 1,
    'merge_single_action' => 1,
    'operations' => array(
      'views_bulk_operations_ruleset_action_rules_ec_complete_shipped_transactions' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_transaction_delete' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_message_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_script_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_argument_selector_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_goto_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_send_email_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_allocation' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow-b5d4b3465e668532a4a53d7ab71057b2' => array(
        'selected' => 1,
        'skip_confirmation' => 0,
        'label' => '',
      ),
    ),
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/store/in_picking');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'In picking',
    'description' => 'List of unshipped orders.',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $views[$view->name] = $view;

  /*
   * View 'ec_transaction_list'
   */
  $view = new view;
  $view->name = 'ec_transaction_list';
  $view->description = 'List of transactions in various stages';
  $view->tag = 'ec_store';
  $view->base_table = 'ec_transaction';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'uid' => array(
      'label' => 'User',
      'required' => 0,
      'id' => 'uid',
      'table' => 'ec_customer',
      'field' => 'uid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'txnid' => array(
      'label' => 'Id',
      'exclude' => 0,
      'id' => 'txnid',
      'table' => 'ec_transaction',
      'field' => 'txnid',
      'relationship' => 'none',
    ),
    'fullname' => array(
      'label' => 'Customer',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => 'user/[uid]',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'include_multiline_address' => 1,
      'id' => 'fullname',
      'table' => 'ec_transaction_address',
      'field' => 'fullname',
      'relationship' => 'none',
    ),
    'formatted_address' => array(
      'label' => 'Address',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'formatted_address',
      'table' => 'ec_transaction_address',
      'field' => 'formatted_address',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Name',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<strong>User:</strong> [name]',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'uid',
    ),
    'description' => array(
      'label' => 'Workflow',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<strong>Workflow:</strong> [description]',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'description',
      'table' => 'ec_workflow_statuses',
      'field' => 'description',
      'relationship' => 'none',
    ),
    'allocation' => array(
      'label' => 'Allocation Status',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<strong>Allocation:</strong> [allocation]',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'allocation',
      'table' => 'ec_transaction',
      'field' => 'allocation',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'd M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_transaction',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'changed' => array(
      'label' => 'Changed',
      'date_format' => 'custom',
      'custom_date_format' => 'd M y',
      'exclude' => 0,
      'id' => 'changed',
      'table' => 'ec_transaction',
      'field' => 'changed',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => 'Balance',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_transaction',
      'field' => 'balance',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'gross' => array(
      'label' => 'Total',
      'exclude' => 0,
      'id' => 'gross',
      'table' => 'ec_transaction',
      'field' => 'gross',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_transaction',
      'field' => 'operations',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'keys' => array(
      'operator' => 'optional',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'keys_op',
        'identifier' => 'keys',
        'label' => 'Search',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'keys',
      'table' => 'search_index',
      'field' => 'keys',
      'relationship' => 'none',
    ),
    'txnid' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'txnid_op',
        'identifier' => 'txnid',
        'label' => 'Transaction id',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'txnid',
      'table' => 'ec_transaction',
      'field' => 'txnid',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'or',
      'value' => array(
        'billing' => 'billing',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'ec_transaction_address',
      'field' => 'type',
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'workflow' => array(
      'operator' => 'in',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'workflow_op',
        'identifier' => 'workflow',
        'label' => 'Workflow',
        'optional' => 1,
        'single' => 0,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'workflow',
      'table' => 'ec_workflow_statuses',
      'field' => 'workflow',
      'relationship' => 'none',
    ),
    'ec_exposed_fieldset' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'ec_exposed_fieldset',
      'table' => 'ec_transaction',
      'field' => 'ec_exposed_fieldset',
      'fields' => array(
        'txnid' => 'txnid',
        'workflow' => 'workflow',
        'erid' => 'erid',
        'keys' => 0,
      ),
      'relationship' => 'none',
      'fieldset_title' => 'Advanced Search',
      'fieldset_collapsible' => 1,
      'fieldset_collapsed' => 1,
    ),
    'erid' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'erid_op',
        'identifier' => 'erid',
        'label' => 'Receipt Id',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'erid',
      'table' => 'ec_receipt_allocation',
      'field' => 'erid',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'store admin manage',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Transactions');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk_multi');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'summary' => '',
    'columns' => array(
      'txnid' => 'txnid',
      'fullname' => 'fullname',
      'formatted_address' => 'fullname',
      'name' => 'fullname',
      'description' => 'description',
      'allocation' => 'description',
      'created' => 'created',
      'changed' => 'changed',
      'balance' => 'balance',
      'gross' => 'gross',
      'operations' => 'operations',
    ),
    'info' => array(
      'txnid' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'fullname' => array(
        'sortable' => 0,
        'separator' => '<br />',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'formatted_address' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'description' => array(
        'sortable' => 1,
        'separator' => '<br />',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'allocation' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'balance' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'gross' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'operations' => array(
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
    ),
    'default' => 'txnid',
    'execution_type' => '1',
    'max_performance' => 0,
    'display_type' => '0',
    'hide_selector' => 0,
    'preserve_selection' => 1,
    'display_result' => 1,
    'merge_single_action' => 1,
    'operations' => array(
      'views_bulk_operations_ruleset_action_rules_ec_complete_shipped_transactions' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_transaction_delete' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_message_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_script_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_argument_selector_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_goto_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_send_email_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_allocation' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow-b5d4b3465e668532a4a53d7ab71057b2' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
    ),
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'summary' => '',
    'columns' => array(
      'txnid' => 'txnid',
      'fullname' => 'fullname',
      'formatted_address' => 'fullname',
      'name' => 'fullname',
      'description' => 'description',
      'allocation' => 'description',
      'created' => 'created',
      'changed' => 'changed',
      'balance' => 'balance',
      'gross' => 'gross',
      'operations' => 'operations',
    ),
    'info' => array(
      'txnid' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'fullname' => array(
        'sortable' => 0,
        'separator' => '<br />',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'formatted_address' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'description' => array(
        'sortable' => 1,
        'separator' => '<br />',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'allocation' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'balance' => array(
        'sortable' => 0,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'gross' => array(
        'sortable' => 1,
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
      'operations' => array(
        'separator' => '',
        'row' => '',
        'rowspan' => '',
        'colspan' => '',
      ),
    ),
    'default' => 'txnid',
    'execution_type' => '1',
    'max_performance' => 0,
    'display_type' => '0',
    'hide_selector' => 0,
    'preserve_selection' => 1,
    'display_result' => 1,
    'merge_single_action' => 1,
    'operations' => array(
      'views_bulk_operations_ruleset_action_rules_ec_complete_shipped_transactions' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_transaction_delete' => array(
        'selected' => 1,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_message_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_script_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'views_bulk_operations_argument_selector_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_goto_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'system_send_email_action' => array(
        'selected' => 0,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_allocation' => array(
        'selected' => 1,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow' => array(
        'selected' => 1,
        'skip_confirmation' => 0,
        'label' => '',
      ),
      'ec_store_action_set_workflow-b5d4b3465e668532a4a53d7ab71057b2' => array(
        'selected' => 1,
        'skip_confirmation' => 0,
        'label' => '',
      ),
    ),
  ));
  $handler->override_option('path', 'admin/store/transaction');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Transactions',
    'description' => 'List/search transactions.',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $views[$view->name] = $view;

  return $views;
}


